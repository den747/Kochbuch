### Buttermilch Waffeln

> 6 Portionen / Vorbereitung: 5 Minuten / Zeit: 15-20 Minuten


| Menge | Art        | Zutat           |
|------:|------------|-----------------|
|   500 | Gramm      | Mehl            |
|   1/2 | Teelöffel  | Salz            |
|     2 | Esslöffel  | Zucker          |
| 1 1/2 | Teelöffel  | Backpulver      |
|   400 | Milliliter | Buttermilch     |
|     2 |            | Eier            |
|    50 | Gramm      | Butter          |
|   1/2 | Teelöffel  | Vanille Extrakt |
|       |            | Margarine       |



1. Mix alle trockene Zutaten
2. Mix die Buttermilch mit dem Eigelb (Eiweiß aufbewahren), rühre die Butter und den Vaille Extrakt drunter
3. Waffeleisen mit Margarine/Öl einstreichen und vorheizen
4. Mix die die trockenen mit den feuchten Zutaten
5. Schlag das Eiweiß bis es anfängt streif zu werden und rühre es Vorsichtig unter den Teig

Der Teig braucht drei bis fünf Minuten auf dem Waffeleisen.

> Source: [Smitten Kitchen](https://smittenkitchen.com/2007/03/again-with-the-pining/)