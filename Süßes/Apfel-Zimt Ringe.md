### Apfel-Zimt Ringe

> 6 Portionen / Vorbereitung: 5 Minuten / Zeit: 15-20 Minuten

| Menge | Art  | Zutat       |
|------:|------|-------------|
|     4 | Stück | Äpfel       |
|   128 | Gram    | Mehl        |
|   1/4 | Teelöffel   | Backpulver  |
|     2 | Esslöffel   | Zucker      |
|   1/4 | Teelöffel   | Salz        |
|   1/4 | Teelöffel   | Zimt        |
|     1 | Stück | Ei          |
|   240 | Millilieter   | Buttermilch |
|       |      | Zimt-Zucker |
|       |      | Fritieröl   |


1. Mix das Mehl, Backpulver, Zucker, Salz und Zimt
2. Mix das Ei mit der Buttermilch, gründlich
3. Die Äpfel in Ringe schneiden (~4 Ringe pro Apfel). Entferne den Kern und trockne die Ringe
4. Erhitze das Öl auf 180°
5. Kombiniere die Mehlmixtur mit der Buttermilch, entferne alle Klumpen
6. Dip die Ringe einzelnt in den Teig. Entferne überschüssigen Teig bevor du sie fritierst
8. Wenn sie beidseitig Gold-Braun sind trockne das überschüssige Öl ab
9. Dip sie einmal in Zimt & Zucker und lass die Ringe auf einem Rost abkühlen (NICHT auf einer flachen ablage!)

Am besten lauwarm!

> Source: [Nextcloud (Video)](https://192.168.1.26:8000/index.php/f/170880)