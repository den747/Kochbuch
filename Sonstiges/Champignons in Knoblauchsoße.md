# Champignons in Knoblauchsoße
> Arbeitszeit: ca. 30 Min. / Schwierigkeitsgrad: normal / Kalorien p. P.: keine Angabe 

![](https://static.chefkoch-cdn.de/ck.de/rezepte/111/111615/447988-420x280-fix-champignons-mit-knoblauchsosse.jpg)

|Menge|Zutat|
|---:|:---|
|1 kg|Lauchzwiebel(n) oder 1 Stange Porree|
|3|Knoblauchzehe(n)|
|150g|Crème fraîche|
|150g|Joghurt, fettarmer|
|1 TL|Thymian, gehackt|
|2 EL|Öl|
|30g|Butter, weiche|
||Salz und Pfeffer|
|1/2|Baguette(s)|

1. Champignons und Lauchzwiebeln putzen und waschen. Die Lauchzwiebeln in Stücke schneiden. Knoblauch schälen und fein hacken.

2. Die Crème fraîche mit Joghurt und der Hälfte vom Knoblauch glatt rühren. Die Hälfte vom Thymian unter die Soße rühren. Mit Salz und Pfeffer abschmecken.

3. Öl in einer großen Pfanne erhitzen. Die Pilze unter Wenden portionsweise kräftig anbraten. Die Lauchzwiebeln kurz mit anbraten. Mit Salz und Pfeffer würzen.

4. Die Butter mit dem restlichen Knoblauch verrühren. Mit Salz und Pfeffer würzen.

5. Das Baguette längs halbieren und mit der Knoblauchbutter bestreichen. Unter dem heißen Grill oder bei höchster Hitze im Backofen ca. 5 Minuten goldbraun überbacken.

6. Etwas Knoblauch-Soße über die Pilze geben und mit dem restlichen Thymian bestreuen. Soße und Baguette dazu reichen. 


> Source: [Chefkoch.de](https://www.chefkoch.de/rezepte/1116151217744829/Champignons-mit-Knoblauchsosse.html)