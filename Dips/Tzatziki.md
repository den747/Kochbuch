# Tzatziki

| #     | Art  | Zutat                |
|------:|------|----------------------|
|       |      | Griechischen Joghurt |
|     2 | Stk. | Gurken               |
|     1 | Pkt. | Quark                |
|     1 | Stk. | Zitrone              |
|     1 | Pkt. | Dill                 |
|       |      | Knoblauch (gerieben) |
|       |      | Salz                 |
|       |      | Pfeffer              |

1. Joghurt in eine Schüssel
2. Entkerne die Gurke, reib sie klein (oder nutz die Küchenmaschine) und press die Flüssigkeit so weit wie möglich raus.
3. Mix alle Zutaten in einer Schüssel.

Für das beste Ergebniss ein paar Stunden ziehen lassen


> Quelle: [Smitten Kitchen](https://smittenkitchen.com/2009/09/grilled-lamb-kebabs-tzatziki/)